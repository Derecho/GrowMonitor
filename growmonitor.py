# Main application, tailored to own hardware
# Not even remotely close to being finished yet
import time
from datetime import datetime, timedelta
import json
import paho.mqtt.publish as publish
import sensors

# Configurable settings
HOST = "10.9.8.3"
INTERVAL = timedelta(minutes=1)
TOPIC = "growmonitor/update"

def setup(context):
    """Set up all sensors and put them in the context"""
    context['bme280'] = sensors.BME280(0)

def update(context):
    """Read all measurements and publish them over MQTT"""
    print(datetime.now())

    temp = context['bme280'].readTemperature()
    press = context['bme280'].readPressure(False)
    hum = context['bme280'].readHumidity(False)

    print("Temperature: {:.2f} C\nPressure: {:.2f} hPa\nHumidity: {:.3f}%\n" \
            .format(temp, press, hum))

    msg = json.dumps({
        "temperature": "{:.2f}".format(temp),
        "pressure": "{:.2f}".format(press),
        "humidity": "{:.3f}".format(hum),
        })
    publish.single(TOPIC, msg, hostname=HOST)

if __name__ == "__main__":
    context = {}
    setup(context)

    trigger_time = datetime.now()
    # Check time every second, or 1/10th of the interval, whichever is shorter
    sleep_time = min([(INTERVAL/10).total_seconds(), 1])

    # Main loop
    while True:
        if trigger_time <= datetime.now():
            trigger_time += INTERVAL
            update(context)
        else:
            time.sleep(sleep_time)
