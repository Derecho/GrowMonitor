# GrowMonitor
GrowMonitor is a personal project intended to collect data to aid in growing
plants. This repository contains the application that is meant to run on my
custom hardware setup. Whilst the code is tailored to my specific situation,
it should be fairly reusable and extendable. The application reads out attached
sensors and exposes the values over MQTT.

## Hardware
GrowMonitor runs on an Orange Pi Zero running Armbian Stretch with a mainline
kernel. It should run on similar ARM-based boards with little or no
adjustments.

The Orange Pi Zero has the following additional components connected to it:

   - I2C
      - BME280: Temperature, humidity and pressure
   - SPI
      - MCP3208: 8 channel ADC for monitoring analogue soil moisture values

The I2C and SPI buses need to be enabled on Armbian as they are not enabled by
default. However, on recent versions there are DTB overlays shipped with the
standard image so enabling these buses only involves adjusting the `overlays`
parameter in `armbianEnv.txt` or using the `armbian-config` tool.

Relevant lines in `/boot/armbianEnv.txt`:

    overlay_prefix=sun8i-h3
    overlays=i2c0 spi-spidev usbhost2 usbhost3
    param_spidev_spi_bus=1

## Dependencies
This application depends on the `smbus`, `spidev` and `paho-mqtt` modules.
`smbus` can be obtained by issuing `apt-get install python-smbus` or by getting
a compatible replacement module from PyPI. `spidev` and `paho-mqtt` are
directly available on PyPI and installable via `pip install <package>`.

## Progress

   - [x] BMP280 support
   - [x] BME280 support
   - [x] MCP3208 support
   - [ ] Soil moisture value interpretation
   - [x] MQTT client exposing available data

## OS configuration
### Udev
In order to avoid having to run the GrowMonitor application as root, you can
place `50-growmonitor.rules` in `/etc/udev/rules.d/`. This allows any user
that's in the `dialout` group to access the required I2C and SPI ports.

### Systemd
You may also want to configure the GrowMonitor application to run automatically
at boot. As Armbian comes with systemd by default an example unit configuration
file is provided in this repository.

Copy `growmonitor.service` into `/etc/systemd/system` and run `sudo systemctl
enable growmonitor` to have systemd run it when the system has started up. This
file assumes you have cloned this repository to `/home/pi`, edit the according
line if this is not the case.

## Home Assistant
GrowMonitor publishes sensor values to an MQTT broker. The values can then
be visualised by subscribing to these messages. One of many compatible clients
is Home Assistant, which is what will be used in this project.

![Grouped measurements](https://git.drk.sc/Derecho/GrowMonitor/raw/master/homeassistant1.png)

![Humidity graph](https://git.drk.sc/Derecho/GrowMonitor/raw/master/homeassistant2.png)

### Configuration
`configuration.yaml`

    recorder:
      include:
        entities:
          - sensor.growmonitor_temperature
          - sensor.growmonitor_pressure
          - sensor.growmonitor_humidity
    
    sensor:
      - platform: mqtt
        name: "GrowMonitor Temperature"
        state_topic: "growmonitor/update"
        unit_of_measurement: "°C"
        value_template: "{{ value_json.temperature }}"
      - platform: mqtt
        name: "GrowMonitor Pressure"
        state_topic: "growmonitor/update"
        unit_of_measurement: "hPa"
        value_template: "{{ value_json.pressure }}"
      - platform: mqtt
        name: "GrowMonitor Humidity"
        state_topic: "growmonitor/update"
        unit_of_measurement: "%"
        value_template: "{{ value_json.humidity }}"

`customize.yaml`

    sensor.growmonitor_pressure:
      icon: mdi:speedometer
    sensor.growmonitor_humidity:
      icon: mdi:water-percent

