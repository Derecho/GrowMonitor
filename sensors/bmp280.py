import smbus
import time

class BMP280(object):
    """I2C-connected BMP280 pressure sensor"""
    # BMP280 register addresses
    DIG = 0x88
    ID = 0xD0
    CTRL_MEAS = 0xF4
    CONFIG = 0xF5
    PRESS = 0xF7
    TEMP = 0xFA

    def __init__(self, bus_id, address=0x76):
        self.bus = smbus.SMBus(bus_id)
        self.address = address

        dev_id = self.readID()
        if dev_id != 0x58:
            raise ValueError("Device sent back wrong ID ({})".format(dev_id))

        self.readTrimmingValues()

    def _ss(self, value):
        """Interpret value as a signed short (2 bytes)"""
        if value >= 2**15:
            return value - 2**16
        return value

    def _sl(self, value):
        """Interpret value as a signed long (4 bytes)"""
        if value >= 2**31:
            return value - 2**32
        return value

    def readID(self):
        return self.bus.read_byte_data(self.address, self.ID)

    def readTrimmingValues(self):
        """Reads and stores the trimming values the sensor was calibrated with at the factory"""
        data = self.bus.read_i2c_block_data(self.address, self.DIG, 24)

        self.dig = {}
        self.dig['T1'] = (data[1] << 8) + data[0]
        self.dig['T2'] = self._ss((data[3] << 8) + data[2])
        self.dig['T3'] = self._ss((data[5] << 8) + data[4])
        self.dig['P1'] = (data[7] << 8) + data[6]
        self.dig['P2'] = self._ss((data[9] << 8) + data[8])
        self.dig['P3'] = self._ss((data[11] << 8) + data[10])
        self.dig['P4'] = self._ss((data[13] << 8) + data[12])
        self.dig['P5'] = self._ss((data[15] << 8) + data[14])
        self.dig['P6'] = self._ss((data[17] << 8) + data[16])
        self.dig['P7'] = self._ss((data[19] << 8) + data[18])
        self.dig['P8'] = self._ss((data[21] << 8) + data[20])
        self.dig['P9'] = self._ss((data[23] << 8) + data[22])

    def triggerMeasurement(self):
        """Trigger a measurement (forced mode)"""
        # Set forced mode, ultra low power oversampling, 1x resolution, no IIR
        self.bus.write_byte_data(self.address, self.CTRL_MEAS, 0x25)
        time.sleep(0.007)  # Wait over the max 6.4ms measurement time

    def readPressure(self, update=True):
        """Obtains pressure measurement data from sensor and converts it to hPa"""
        if update:
            self.readTemperature()  # readTemperature will trigger a measurement, and calculate a new t_fine

        data = self.bus.read_i2c_block_data(self.address, self.PRESS, 3)
        raw = (data[0] << 12) + (data[1] << 4) + (data[2] >> 4)

        # Calculation as per datasheet
        var1 = self.t_fine - 128000;
        var2 = var1 * var1 * self.dig['P6']
        var2 += (var1 * self.dig['P5']) << 17
        var2 += self.dig['P4'] << 35
        var1 = ((var1 * var1 * self.dig['P3']) >> 8) \
                + ((var1 * self.dig['P2']) << 12)
        var1 = ( ((1 << 47) + var1) * self.dig['P1']) >> 33
        if var1 == 0:
            return 0
        p = 1048576 - raw
        p = ( ((p << 31) - var2) * 3125) / var1
        var1 = (self.dig['P9'] * (p >> 13) * (p >> 13)) >> 25
        var2 = (self.dig['P8'] * p) >> 19
        p = ((p + var1 + var2) >> 8) + (self.dig['P7'] << 4)
        return round(p/25600.0, 2) # Convert from Pa in Q24.8 to hPa in float

    def readTemperature(self, update=True):
        """Obtains temperature measurement data from sensor and converts it to degrees Celcius"""
        if update:
            self.triggerMeasurement()

        data = self.bus.read_i2c_block_data(self.address, self.TEMP, 3)
        raw = (data[0] << 12) + (data[1] << 4) + (data[2] >> 4)

        # Sample values to check formula with, should output 25.08
        #raw = 519888
        #self.dig['T1'] = 27504
        #self.dig['T2'] = 26435
        #self.dig['T3'] = -1000

        # Calculation as per datasheet
        var1 = (((raw >> 3) - (self.dig['T1'] << 1)) * self.dig['T2']) >> 11
        var2 = ( (( ((raw >> 4) - self.dig['T1'])
                * ((raw >> 4) - self.dig['T1'])) >> 12)
                * self.dig['T3'] ) >> 14
        self.t_fine = var1 + var2
        return round((((self.t_fine * 5) + 128) >> 8) / 100.0, 2)
