from bmp280 import BMP280
import smbus
import time

# Extending BMP280 as functionality for pressure and temperature is compatible
class BME280(BMP280):
    """I2C-connected BME280 environmental sensor"""
    DIG_H1 = 0xA1
    DIG_H2 = 0xE1
    CTRL_HUM = 0xF2
    HUM = 0xFD

    def __init__(self, bus_id, address=0x76):
        self.bus = smbus.SMBus(bus_id)
        self.address = address

        dev_id = self.readID()
        if dev_id != 0x60:
            raise ValueError("Device sent back wrong ID ({})".format(dev_id))

        self.readTrimmingValues()

        # Whilst osrs_p and osrs_t are set during the command to set forced
        # mode, osrs_h is on another address and needs to be set separately.
        self.bus.write_byte_data(self.address, self.CTRL_HUM, 0x01)
    
    def _sc(self, value):
        """Interpret value as a signed char (1 byte)"""
        if value >= 2**7:
            return value - 2**8
        return value
    
    def triggerMeasurement(self):
        """Trigger a measurement (forced mode)"""
        super(BME280, self).triggerMeasurement()
        # More time required than the BMP280 due to humidity measurement,
        # wait another 5ms on top of the existing 7ms
        time.sleep(0.005)

    def readTrimmingValues(self):
        """Reads and stores the trimming values the sensor was calibrated with at the factory"""
        # dig_T* and dig_P* are at the same locations as with the BMP280
        super(BME280, self).readTrimmingValues()

        self.dig['H1'] = self.bus.read_byte_data(self.address, self.DIG_H1)

        data = self.bus.read_i2c_block_data(self.address, self.DIG_H2, 7)
        self.dig['H2'] = self._ss((data[1] << 8) + data[0])
        self.dig['H3'] = data[2] 
        self.dig['H4'] = self._ss((data[3] << 4) + (data[4] & 0x0F))
        self.dig['H5'] = self._ss((data[5] << 4) + (data[4] >> 4))
        self.dig['H6'] = self._sc(data[6])

    def readHumidity(self, update=True):
        """Obtains relative humidity data from sensor and converts it to a percentage"""
        if update:
            self.readTemperature()  # readTemperature will trigger a measurement, and calculate a new t_fine

        data = self.bus.read_i2c_block_data(self.address, self.HUM, 2)
        raw = (data[0] << 8) + data[1]

        # Calculation as per datasheet
        # Indentation is meant to help readability, ymmv
        v_x1_u32r = self.t_fine - 76800
        v_x1_u32r = (
                (( ((raw << 14) - (self.dig['H4'] << 20)
                    - (self.dig['H5'] * v_x1_u32r)) + 16384) >> 15)
                * (
                    (
                      (
                        ( ((v_x1_u32r * self.dig['H6']) >> 10)
                            * ((v_x1_u32r * self.dig['H3'] >> 11) + 32768) >> 10)
                        + 2097152
                      )
                      * self.dig['H2'] + 8192
                    )
                    >> 14
                  )
                )
        v_x1_u32r -= ( 
                  ( 
                    ( ((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) \
                    * self.dig['H1']
                  )
                  >> 4
                )

        if v_x1_u32r < 0:
            v_x1_u32r = 0
        if v_x1_u32r > 419430400:
            v_x1_u32r = 419430400

        return round((v_x1_u32r >> 12)/1024.0, 3)
