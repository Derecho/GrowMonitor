import spidev

class MCP3208(object):
    def __init__(self, bus, device, Vref=3.3):
        """SPI-connected MCP3208 ADC"""
        self.spi = spidev.SpiDev(bus, device)
        # Explicitely use 1MHz (default anyway)
        self.spi.max_speed_hz = 1000000
        self.Vref = Vref

    def readChannel(self, channel, samples=1):
        """Obtains a channel reading from sensor and converts it to a voltage"""
        if samples > 1:
            values = [self.readChannel(channel) for i in range(samples)]
            return sum(values)/len(values)

        if channel > 7:
            raise ValueError("Invalid channel: {}".format(channel))

        data_tx = [0x04, 0x00, 0x00]  # Start bit set
        data_tx[0] |= channel >> 2
        data_tx[1] |= (channel & 0b00000011) << 6

        data_rx = self.spi.xfer2(data_tx)
        return (((data_rx[1] & 0b00001111) << 8) | data_rx[0]) \
                * self.Vref / 4096.0

    def testStability(self, channel, samples=1, count=1000):
        """Performs basic statistics on repeated readings"""
        buf = []
        for i in range(count):
            buf += [self.readChannel(channel, samples)]

        print("Min: {}".format(min(buf)))
        print("Max: {}".format(max(buf)))
        mean = sum(buf)/len(buf)
        print("Mean: {}".format(mean))
        # Line below as per DBrowne's comment on
        # https://stackoverflow.com/questions/15389768/standard-deviation-of-a-list
        from math import sqrt
        stddev = sqrt(sum((x - mean)**2 for x in buf) / len(buf))
        print("Standard deviation: {}".format(stddev))
